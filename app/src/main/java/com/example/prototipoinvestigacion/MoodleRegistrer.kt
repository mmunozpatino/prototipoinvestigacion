package com.example.prototipoinvestigacion

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_moodle_registrer.*
import java.net.URL
import android.R.string
import android.content.Context
import android.opengl.Visibility
import android.os.AsyncTask.execute
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.prototipoinvestigacion.data.db.entity.MoodleRoomDatabase
import com.example.prototipoinvestigacion.data.db.entity.user.User
import com.example.prototipoinvestigacion.data.db.entity.user.UserDao
import kotlinx.android.synthetic.main.activity_next_activity.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.util.regex.Pattern

//regex only letters --> [a-zA-Z]

class MoodleRegistrer : AppCompatActivity(), View.OnClickListener {





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_moodle_registrer)

        initUI()


    }

    private fun initUI(){
        err_et_name.visibility = View.GONE
        err_et_apellido.visibility = View.GONE
        err_et_email.visibility = View.GONE
        err_et_apellido_empty.visibility = View.GONE
        err_et_email_empty.visibility = View.GONE
        err_et_legajo_empty.visibility = View.GONE
        err_et_name_empty.visibility = View.GONE
        err_et_pass_empty.visibility = View.GONE
        err_et_username_empty.visibility = View.GONE
        btn_singin.setOnClickListener(this)
    }




    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_singin -> {

                if(isFormValid()){
                    var client = OkHttpClient()


                    val formBody = FormBody.Builder()
                        .add("user_moodle_name", et_username.text.toString())
                        .add("user_moodle_password",et_password.text.toString())
                        .add("legajo",et_legajo.text.toString())
                        .add("nombre",et_name.text.toString())
                        .add("apellido",et_apellido.text.toString())
                        .add("email",et_email.text.toString())
                        .build()

                    val request = Request.Builder()
                        .url("http://plataforma.i-sis.com.ar/ecau/registro")
                        .post(formBody)
                        .build()


                    client.newCall(request).enqueue(object: Callback {
                        override fun onFailure(call: Call, e: IOException) {

                            //para volver al hilo principal!
                            runOnUiThread {
                                Toast.makeText(applicationContext,"El usuario ingresado no existe en Moodle!",Toast.LENGTH_LONG).show()
                            }
                        }

                        override fun onResponse(call: Call, response: Response) {
                            var code = response.code()

                            //para volver al hilo principal!
                            runOnUiThread {

                                if (code == 200) {
                                    var user = User(
                                        et_username.text.toString(),
                                        et_password.text.toString(),
                                        et_email.text.toString(),
                                        et_name.text.toString(),
                                        et_apellido.text.toString(),
                                        et_legajo.text.toString()
                                    )
                                    val db = MoodleRoomDatabase(this@MoodleRegistrer)

                                    db.userDao().insert(user)

                                    val sharedPreference = getSharedPreferences("Moodle", Context.MODE_PRIVATE)
                                    var editor = sharedPreference.edit()
                                    editor.putString("username", et_username.text.toString())
                                    editor.apply()
                                    var intent = Intent(this@MoodleRegistrer, MapsLoginActivity::class.java)
                                    intent.putExtra("username", et_username.text.toString())
                                    startActivity(intent)
                                }
                            }
                        }
                    })
                }


            }
        }
    }

    private fun isFormValid(): Boolean{
        //check username
        if(et_username.text.toString() != ""){
            err_et_username_empty.visibility = View.GONE
        }else{
            err_et_username_empty.visibility = View.VISIBLE
        }

        //check password
        if(et_password.text.toString() != ""){
            err_et_pass_empty.visibility = View.GONE
        }else{
            err_et_pass_empty.visibility = View.VISIBLE
        }

        //check legajo
        if(et_legajo.text.toString() != ""){
            err_et_legajo_empty.visibility = View.GONE
        }else{
            err_et_legajo_empty.visibility = View.VISIBLE
        }

        //check name
        if(et_name.text.toString() != ""){
            err_et_name_empty.visibility = View.GONE
            if(hasOnlyLetters(et_name.text.toString())){
               err_et_name.visibility = View.GONE
            }else{
                err_et_name.visibility = View.VISIBLE
            }
        }else{
            err_et_name_empty.visibility = View.VISIBLE
        }

        //check lastname
        if(et_apellido.text.toString() != ""){
            err_et_apellido_empty.visibility = View.GONE
            if(hasOnlyLetters(et_apellido.text.toString())){
                err_et_apellido.visibility = View.GONE
            }else{
                err_et_apellido.visibility = View.VISIBLE
            }
        }else{
            err_et_apellido_empty.visibility = View.VISIBLE
        }

        //check email
        if(et_email.text.toString() != ""){
            err_et_email_empty.visibility = View.GONE
            if(isValidEmail(et_email.text.toString())){
                err_et_email.visibility = View.GONE
            }else{
                err_et_email.visibility = View.VISIBLE
            }
        }else{
            err_et_email_empty.visibility = View.VISIBLE
        }

        return !isFormIncomplete() && !hasFormErrors()
    }

    private fun isFormIncomplete (): Boolean{
        return et_username.text.toString() == "" || et_password.text.toString() == "" || et_legajo.text.toString() == "" || et_name.text.toString() == "" || et_apellido.text.toString() == "" || et_email.text.toString() == ""
    }

    private fun hasFormErrors(): Boolean{
        return !hasOnlyLetters(et_name.text.toString()) || !hasOnlyLetters(et_apellido.text.toString()) || !isValidEmail(et_email.text.toString())
    }

    private fun hasOnlyLetters(word: String): Boolean{
        val onlyLetters = "([a-zA-Z]|[áéíóúÁÉÍÓÚ])+".toRegex()
        return onlyLetters.matches(word)
    }

    private fun isValidEmail(email: String): Boolean{
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }




}

