package com.example.prototipoinvestigacion

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.provider.ContactsContract.Directory.PACKAGE_NAME
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.prototipoinvestigacion.ui.NextActivityActivity
import kotlinx.android.synthetic.main.activity_moodle_login.*
import com.example.prototipoinvestigacion.Utils
import com.example.prototipoinvestigacion.LocationUpdatesService
import com.example.prototipoinvestigacion.data.db.entity.MoodleRoomDatabase
import com.example.prototipoinvestigacion.data.db.entity.user.User
import java.text.DateFormat
import java.util.*



class MoodleLogin : AppCompatActivity(), View.OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {



    lateinit var username: String
    lateinit var sharedPreference: SharedPreferences


    private val TAG = MainActivity::class.java.simpleName

    // Used in checking for runtime permissions.
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    // The BroadcastReceiver used to listen from broadcasts from the service.
//    private var myReceiver: MyReceiver? = null

    // A reference to the service used to get location updates.
    private var mService: LocationUpdatesService? = null

    // Tracks the bound state of the service.
    private var mBound = false

    // UI elements.
    private var mRequestLocationUpdatesButton: Button? = null
    private var mRemoveLocationUpdatesButton: Button? = null


    private lateinit var user: User

    private val mServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_moodle_login)

        requestPermissions()
        btn_singin.setOnClickListener(this)
        btn_login.setOnClickListener(this)

        sharedPreference =  getSharedPreferences("Moodle", Context.MODE_PRIVATE)

        username = sharedPreference.getString("username","")

        Log.i("mechi", "username on shared -> " + username)
        if(username != ""){

                val intent = Intent(this, NextActivityActivity::class.java)
                startActivity(intent)
        }else{
            val intent = Intent(this, MoodleRegistrer::class.java)
            startActivity(intent)
        }

        if (PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean("requesting_locaction_updates", false)) {
            if (!checkPermissions()) {
                requestPermissions()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)
    }

    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }



    override fun onClick(v: View?) {

        when(v?.id){
            R.id.btn_singin -> {
                var intent = Intent(this,MoodleRegistrer::class.java)
                startActivity(intent)

            }
            R.id.btn_login -> {
                val usr = et_username.text.toString()

                Log.i("mechi", "Login -> "+usr)

                if(usr != ""){
                    val db = MoodleRoomDatabase(this)
                    db.userDao().deleteAllUsers()

                    var userRoom =  db.userDao().getByUsername(username)

                    Log.i("mechi", "conseguí al usuario -> " + userRoom.size)


                    if(userRoom.size != 0){
                        Toast.makeText(this,"Inicio de sesión exitoso!", Toast.LENGTH_SHORT).show()
                        var editor = sharedPreference.edit()
                        editor.putString("username",usr)
                        editor.apply()
                        intent.putExtra("username", usr)
                        startActivity(intent)
                    }else{
                        Toast.makeText(this,"Error! Nombre de usuario incorrecto!", Toast.LENGTH_SHORT).show()

                    }

                }

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "se denegaron los permisos", Toast.LENGTH_SHORT).show()

            }
        }
    }


    override fun onStop() {
        if (mBound) {
            unbindService(mServiceConnection)
            mBound = false
        }
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)
        super.onStop()    }





    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "requesting_locaction_updates") {

        }
    }

    private fun requestPermissions(){
        if (ActivityCompat
                .checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION),
                34
            )
        }
    }


}
