package com.example.prototipoinvestigacion

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.service.voice.VoiceInteractionSession
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.prototipoinvestigacion.data.db.entity.MoodleRoomDatabase
import com.example.prototipoinvestigacion.data.db.entity.position.Position
import com.example.prototipoinvestigacion.ui.NextActivityActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.activity_moodle_registrer.*

class LocationUpdatesService: Service() {



    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private var lastMarker: Marker? = null
    private var previousMarker: Marker? = null
    private var level: Int? = 0
    private var isNotify: Boolean = false

    //notificacion
    private lateinit var notificationManager: NotificationManager
    private val ID_NOTIFICATION = 123

    //preferencias
    private lateinit var preferences: SharedPreferences
    private var PREFERENCE_NAME = "shared preferences"

    private val mBinder = LocalBinder()

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTING = 2
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    private val CHANNEL_ID = "channel_01"

    private var mNotificationManager: NotificationManager? = null

    private val NOTIFICATION_ID = 12345678

    private lateinit var sharedPreference: SharedPreferences

    private var lastCoor: Int = 0

    private lateinit var positions: List<Position>

    private lateinit var username: String

    override fun onCreate() {
        super.onCreate()
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        sharedPreference =  getSharedPreferences("Moodle", Context.MODE_PRIVATE)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                onNewLocation(p0.lastLocation)
            }

        }
        createLocationRequest()
    }

    private fun startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//            Activity

        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 1500
        locationRequest.fastestInterval = 1000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("mechi", "on destroy!")
        mNotificationManager!!.cancel(456)
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    inner class LocalBinder : Binder() {
        internal val service: LocationUpdatesService
            get() = this@LocationUpdatesService
    }

    fun onNewLocation(newLocation: Location){

        username = sharedPreference.getString("username", "")!!

        val db = MoodleRoomDatabase(this)

        positions = db.positionDao().findByUsername(username)

        lastCoor = sharedPreference.getInt("lastCoor",0)

        var actualCoor = Position(1, 0.0, 0.0, "")
        var range = 0.002
        if(positions.size == 0){
            Toast.makeText(this, "No hay coordenadas registradas!", Toast.LENGTH_LONG).show()
        }else{
            actualCoor= positions[lastCoor]

            range = 0.002
        }



        Log.i("mechi" ," -------------------------------- ")
        val sharedPreference = getSharedPreferences("Moodle", Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        if(actualCoor.longitude - range <= newLocation.longitude && newLocation.longitude <= actualCoor.longitude + range && actualCoor.latitude - range <= newLocation.latitude && newLocation.latitude <= actualCoor.latitude + range ){
            Log.i("mechi", "EN RANGO!")
            mNotificationManager!!.notify(123, getNotification("En rango!"))
            editor.putBoolean("isInRange", true)
        }else{
            mNotificationManager!!.cancel(123)
            editor.putBoolean("isInRange", false)
        }

        editor.apply()

        if(serviceIsRunningInForeground(this)){
            mNotificationManager!!.notify(456, getNotification("Escuchando..."))

        }
    }

    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service in manager.getRunningServices(
            Integer.MAX_VALUE
        )) {
            if (javaClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    private fun getNotification(text: String): Notification {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "pid"
            val descriptionText = "canal moodle go"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("pid", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }


        val intentStop = Intent(this, NextActivityActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        intentStop.putExtra("action", "stopService")
        val pendingIntentStop: PendingIntent = PendingIntent.getActivity(this, 0, intentStop, 0)

        val intent = Intent(this, MapsLoginActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val builder = NotificationCompat.Builder(this, "pid")
            .setSmallIcon(R.drawable.gps_white)
            .setContentTitle("Prototipo Investigación")
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
//            .addAction(R.drawable.gps_white,"Detener", pendingIntentStop)
            .setOngoing(true)
            .setAutoCancel(true)

//        with(NotificationManagerCompat.from(this)) {
//            // notificationId is a unique int for each notification that you must define
//            notify(123, builder.build())
//        }
        return builder.build()
    }
}