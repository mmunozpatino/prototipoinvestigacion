package com.example.prototipoinvestigacion

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.prototipoinvestigacion.data.db.entity.user.User
import com.example.prototipoinvestigacion.ui.adapters.UserAdapter

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = UserAdapter(this)
        recyclerView.adapter = adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            newUserActivityRequestCode -> {
                if(resultCode == Activity.RESULT_OK){
                    data?.let {
                        val user = it.getSerializableExtra("user") as User
                        Log.i("user","email " + user.email)
//                        val user = User(it.getStringExtra("email"),it.getStringExtra("pass"))

//                        userViewModel.insert(user)
                    }
                }
            }
        }
        if (requestCode == newUserActivityRequestCode && resultCode == Activity.RESULT_OK) {

        } else {
            Toast.makeText(
                applicationContext,
                "no se guardo!",
                Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        const val newUserActivityRequestCode = 1
    }
}
