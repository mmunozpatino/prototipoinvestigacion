package com.example.prototipoinvestigacion.data.db.entity.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao{
    @Query("SELECT * from user")
    fun getAllUsers(): LiveData<List<User>>

    @Insert
    fun insert(user: User)

    @Query("SELECT * from user WHERE username LIKE :username")
    fun getByUsername(username: String): List<User>

    @Query("DELETE FROM user")
    fun deleteAllUsers()
}