package com.example.prototipoinvestigacion.data.db.entity.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.prototipoinvestigacion.data.db.entity.position.Position
import com.google.android.gms.maps.model.LatLng
import java.io.Serializable
import java.sql.Array

@Entity(tableName = "user")
class User(
    @ColumnInfo(name="username") var username: String,
    @ColumnInfo(name="password") var password: String,
    @ColumnInfo(name="email") var email: String,
    @ColumnInfo(name="name") var name: String,
    @ColumnInfo(name="last_name") var last_name: String,
    @ColumnInfo(name="legajo") var legajo: String
    ) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name= "id")
    var id: Int = 0
}