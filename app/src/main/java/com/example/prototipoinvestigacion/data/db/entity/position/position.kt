package com.example.prototipoinvestigacion.data.db.entity.position

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "postions")
data class Position(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "latitude") var latitude: Double,
    @ColumnInfo(name= "longitude") var longitude: Double,
    @ColumnInfo(name="username") var username: String
    )