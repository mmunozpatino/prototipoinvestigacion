package com.example.prototipoinvestigacion.data.db.entity

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.prototipoinvestigacion.data.db.entity.position.Position
import com.example.prototipoinvestigacion.data.db.entity.position.PositionDao
import com.example.prototipoinvestigacion.data.db.entity.user.User
import com.example.prototipoinvestigacion.data.db.entity.user.UserDao


@Database(entities = [User::class, Position::class], version = 2)
abstract class MoodleRoomDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun positionDao(): PositionDao

    //lo hacemos singleton
    companion object {
        @Volatile private var instance: MoodleRoomDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            MoodleRoomDatabase::class.java, "moodle.db").allowMainThreadQueries().
            fallbackToDestructiveMigration().build()
    }




}