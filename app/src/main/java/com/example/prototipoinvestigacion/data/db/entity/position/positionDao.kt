package com.example.prototipoinvestigacion.data.db.entity.position

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PositionDao{
    @Query("SELECT * FROM postions")
    fun getAll(): List<Position>

    @Query("SELECT * FROM postions WHERE username LIKE :username")
    fun findByUsername(username: String): List<Position>

    @Insert
    fun insert(position: Position)

    @Query("DELETE FROM postions")
    fun deleteAll()
}