package com.example.prototipoinvestigacion.ui

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
//import com.example.prototipoinvestigacion.R
import kotlinx.android.synthetic.main.activity_moodle_registrer.*
import kotlinx.android.synthetic.main.activity_next_activity.*
import okhttp3.*
import android.os.AsyncTask.execute
import android.util.JsonReader
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.example.prototipoinvestigacion.LocationUpdatesService
import com.example.prototipoinvestigacion.MoodleLogin
import com.example.prototipoinvestigacion.R
import com.example.prototipoinvestigacion.data.db.entity.MoodleRoomDatabase
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.web_view.*
import kotlinx.serialization.json.JSON
import kotlinx.serialization.json.json
//import com.example.prototipoinvestigacion.MapsLoginActivity
import org.json.JSONObject
import java.io.IOException


class NextActivityActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var notificationManager: NotificationManager
    private val ID_NOTIFICATION = 123
    private var resourceUrl : String = ""
    lateinit var username: String
    lateinit var sharedPreference: SharedPreferences
    var isInRange: Boolean = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_next_activity)

        val action = intent?.extras?.getString("action")
        Log.i("mechi" ,"action on extras -> " + action)

        when(action){
            "stopService" -> {
                val service = Intent(this, LocationUpdatesService::class.java)
                stopService(service)
            }
        }

        sharedPreference =  getSharedPreferences("Moodle", Context.MODE_PRIVATE)



        if( sharedPreference.getString("username","") != ""){
            username =  sharedPreference.getString("username","")!!
        }else{
            username = intent.getStringExtra("username")
        }

        isInRange = sharedPreference.getBoolean("isInRange",false)

        Log.i("mechi","Shared de si esta en rango ---> " + isInRange.toString())
        btn_nextResource_1.isEnabled = isInRange
        if(isInRange){
            btn_nextResource_1.visibility = View.VISIBLE
        }else{
            btn_nextResource_1.visibility = View.GONE
        }

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //RECURSO
        btn_nextResource_1.setOnClickListener(this)

        //LOGOUT
        btn_logout.setOnClickListener(this)

        if(!runtime_permission()){
            Log.i("mechi", "permisos ok")
            startService()
        }
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when(v?.id){
//            R.id.bt_nextAct -> {
//                val client = OkHttpClient()
//
//
//                val formBody = FormBody.Builder()
//                    .add("user_moodle_name", username)
//                    .build()
//
//                val request = Request.Builder()
//                    .url("http://plataforma.i-sis.com.ar/ecau/proximoRecursoParams")
//                    .post(formBody)
//                    .build()
//
//                client.newCall(request).enqueue(object: Callback {
//                    override fun onFailure(call: Call, e: IOException) {
//                        Toast.makeText(this@NextActivityActivity, "Ocurrió un error, intenta de nuevo por favor", Toast.LENGTH_LONG).show()
//                    }
//
//                    override fun onResponse(call: Call, response: Response) {
//                        when (response.code()){
//                            200 ->  {
//                                val json = JSONObject(response.body()?.string())
//                                resourceUrl = json["uri"].toString()
////                                runOnUiThread {
////                                    tv_response.text = json["uri"].toString()
////                                }
//                            }
//                            else -> {
//                                runOnUiThread {
//                                    Toast.makeText(
//                                        this@NextActivityActivity,
//                                        "Lo sentimos algo salió mal obteniendo el recurso, intena de nuevo!" + response.code().toString(),
//                                        Toast.LENGTH_SHORT
//                                    ).show()
//                                }
//                            }
//
//                        }
//
//
//
//                    }
//
//
//                })
//            }
//            R.id.btn_push -> {
//
//                val db = MoodleRoomDatabase(this)
//
//                var positions = db.positionDao().findByUsername(username)
//
//                Log.i("mechi", "encontre la posicion -> "+ positions.size + " ubicaciones para " + username)
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                    val name = "pid"
//                    val descriptionText = "canal de mechi"
//                    val importance = NotificationManager.IMPORTANCE_DEFAULT
//                    val channel = NotificationChannel("pid", name, importance).apply {
//                        description = descriptionText
//                    }
//                    // Register the channel with the system
//                    val notificationManager: NotificationManager =
//                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//                    notificationManager.createNotificationChannel(channel)
//                }
//
//                val intent = Intent(this, MoodleLogin::class.java).apply {
//                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                }
//                val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
//
//                val builder = NotificationCompat.Builder(this, "pid")
//                    .setSmallIcon(R.drawable.notification)
//                    .setContentTitle("My notification")
//                    .setContentText("Hello World!")
//                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                    // Set the intent that will fire when the user taps the notification
//                    .setContentIntent(pendingIntent)
//                    .setAutoCancel(true)
//
//                with(NotificationManagerCompat.from(this)) {
//                    // notificationId is a unique int for each notification that you must define
//                    notify(123, builder.build())
//                }
//
//
//            }
            R.id.btn_nextResource_1 -> {
                val client = OkHttpClient()


                val formBody = FormBody.Builder()
                    .add("user_moodle_name", username)
                    .build()

                val request = Request.Builder()
                    .url("http://plataforma.i-sis.com.ar/ecau/proximoRecursoParams")
                    .post(formBody)
                    .build()

                client.newCall(request).enqueue(object: Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Toast.makeText(this@NextActivityActivity, "Ocurrió un error, intenta de nuevo por favor", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        when (response.code()){
                            200 ->  {
                                var json = JSONObject(response.body()?.string())
                                resourceUrl = json["uri"].toString()
//                                runOnUiThread {
//                                    tv_response.text = json["uri"].toString()
//                                    Log.i("mechi", "respuesta: "+json)
//                                }
                            }
                            else -> {
                                runOnUiThread {
                                    Log.e("mercedes", "peticion error " + response.code().toString())
                                    Toast.makeText(
                                        this@NextActivityActivity,
                                        "Lo sentimos algo salió mal obteniendo el recurso, intenta de nuevo!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                    }
                })
                val openURL = Intent(android.content.Intent.ACTION_VIEW)

                if( resourceUrl != ""){
                    openURL.data = Uri.parse(resourceUrl)
                    startActivity(openURL)
                }

            }
            R.id.btn_logout -> {
//                var editor = sharedPreference.edit()
//                editor.putString("username","")
//                editor.apply()
//                var intent = Intent(this, MoodleLogin::class.java)
//                startActivity(intent)
                val db = MoodleRoomDatabase(this)
                db.userDao().deleteAllUsers()
                db.positionDao().deleteAll()
                val intent = Intent(this, LocationUpdatesService::class.java)
                stopService(intent)
                var editor = sharedPreference.edit()
                editor.putString("username", "")
                editor.apply()
                Toast.makeText(this, "Datos limpiados, cerrar aplicación", Toast.LENGTH_SHORT).show()
            }
        } //To change body of created functions use File | Settings | File Templates.
    }


    fun getData(){
        val client = OkHttpClient()
        val body = "mlima"

        val request = Request.Builder()
            .url("http://plataforma.i-sis.com.ar/ecau/actividad")
            .post(RequestBody.create(
                MediaType.parse("text/text-plain"), body
            ))
            .build()
        var response = client.newCall(request)
        response.enqueue(object: Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.i("mechi","onFailure")

                //para volver al hilo principal!
                runOnUiThread {
                    call
                    Toast.makeText(applicationContext,"Error!", Toast.LENGTH_LONG).show()

                }
            }

            override fun onResponse(call: Call, response: Response) {
//                Log.i("mechi","onResponse")
                var code = response.code()

//                JSONObject(response.body().toString())

//                var json = JSONObject(response.body().toString())
                //para volver al hilo principal!
                var json = JSONObject(response.body()?.string())
                runOnUiThread {

//                    var json = JSONObject(response.body().toString())
                    Toast.makeText(applicationContext,"Código de respuesta: "+code, Toast.LENGTH_LONG).show()

//                    Log.i("mechi", "response -> "+ json)
                    Log.i("mechi", "response -> "+  json)
                }
            }

        })
    }

    private fun runtime_permission(): Boolean{
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION) ,100)
            return true
        }
        return false
    }

    private fun startService(){
        val service = Intent(this, LocationUpdatesService::class.java)
        startService(service)
        Toast.makeText(this,"Servicio iniciado",Toast.LENGTH_SHORT).show()
    }
}
