package com.example.prototipoinvestigacion.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.prototipoinvestigacion.R
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.markers_list_item.view.*
import java.util.ArrayList

class MakersListAdapter(val items: ArrayList<LatLng>, val context: Context) : RecyclerView.Adapter<MakersListAdapter.MarkersHolder>()  {


    override fun onBindViewHolder(holder: MarkersHolder, position: Int) {
        holder.txtLat.text = items[position].latitude.toString()
        holder.txtLong.text = items[position].longitude.toString() + ", "
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MakersListAdapter.MarkersHolder {
        var view = LayoutInflater.from(context)?.inflate(R.layout.markers_list_item,p0,false)
        var vh = MarkersHolder(view!!)
        return vh
    }

    class MarkersHolder(v: View) : RecyclerView.ViewHolder(v) {
        private var view: View = v
        var txtLat: TextView
        var txtLong: TextView

        init{
            txtLat = view.txt_lat
            txtLong = view.txt_long
        }
    }
}