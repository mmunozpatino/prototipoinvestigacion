package com.example.prototipoinvestigacion

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.prototipoinvestigacion.classes.Coordenada
import com.example.prototipoinvestigacion.data.db.entity.MoodleRoomDatabase
import com.example.prototipoinvestigacion.data.db.entity.position.Position
import com.example.prototipoinvestigacion.ui.NextActivityActivity
import com.example.prototipoinvestigacion.ui.adapters.MakersListAdapter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_maps_login.*
import kotlinx.android.synthetic.main.activity_moodle_registrer.*
import okhttp3.*
import java.io.IOException
import java.util.ArrayList
import okhttp3.OkHttpClient
import okhttp3.FormBody
import okhttp3.RequestBody






//TODO: Mostrar lista de puntos
//TODO: Mostrar cuantos puntos quedan por cargar
//TODO: Guardar los puntos en la bd

class MapsLoginActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {


    private lateinit var mMap: GoogleMap
    private var marker: Marker? = null
    private lateinit var prevMarker: Marker
    private var positions: ArrayList<LatLng> = ArrayList()

    private var coordenadas: ArrayList<Coordenada> = ArrayList()

    private var pointsAdded = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_login)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        add.setOnClickListener(this)
        btn_registrer.setOnClickListener(this)
        btn_cancel_maps.setOnClickListener(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val frm = LatLng(-32.8970762,-68.8553591)

        // The camera is set to the university here, without a marker
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(frm, 15.0f))

        mMap.setOnMapLongClickListener { latLng ->
            marker = mMap.addMarker(MarkerOptions().position(latLng))
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.add -> {
                registrer(marker!!.position.latitude, marker!!.position.longitude)
            }
            R.id.btn_registrer -> {

                if(pointsAdded >= 5){
                    var intent = Intent(this, MoodleLogin::class.java)
                    startActivity(intent)
                }else{
                    Toast.makeText(this, "Debes registrar al menos 5 puntos!", Toast.LENGTH_SHORT).show()
                }


            }

            R.id.btn_cancel_maps -> {
                var intent = Intent(this, MoodleLogin::class.java)
                startActivity(intent)
            }
        }
    }

    fun registrer(lat: Double, long: Double){

        val client = OkHttpClient()


        val username = intent.getStringExtra("username")

        val formBody = FormBody.Builder()
            .add("user_moodle_name", username)
            .add("latitud", lat.toString())
            .add("longitud", long.toString())
            .build()

        val request = Request.Builder()
            .url("http://plataforma.i-sis.com.ar/ecau/coordenadasParams")
            .post(formBody)
            .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Toast.makeText(this@MapsLoginActivity, "Ocurrió un error, intenta de nuevo por favor", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call, response: Response) {
                when (response.code()){
                    200 ->  {
                        runOnUiThread {
                            positions.add(marker!!.position)
                            var coor = Coordenada(marker!!.position.latitude, marker!!.position.longitude)
                            coordenadas.add(coor)

                            val sharedPreference =  getSharedPreferences("Moodle", Context.MODE_PRIVATE)

                            var username = intent.getStringExtra("username")
                            var position = Position(0, marker!!.position.latitude, marker!!.position.longitude,username )

                            val db = MoodleRoomDatabase(this@MapsLoginActivity)

                            db.positionDao().insert(position)

                            et_length.text = "Has seleccionado " + positions.size.toString() + " puntos! "
                            var editor = sharedPreference.edit()
                            marker?.remove()
                            pointsAdded += 1
                            Toast.makeText(this@MapsLoginActivity, "Coordenada registrada! ", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else -> {
                        runOnUiThread {
                            Toast.makeText(
                                this@MapsLoginActivity,
                                "Algo salió mal, por favor intenta de nuevo!" + response.code().toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }//OK



            }


        })
//    }
    }

}
